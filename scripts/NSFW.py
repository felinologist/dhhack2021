import requests
import os
import wget
from shutil import move,copy
import csv
import pandas as pd
import keys as keyring

debug = True

img_path = 'data'    

meta = 'meta\memes.csv'
meta_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', meta))
const_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', img_path))

def find_top(df):
    leaders_votes = df.nlargest(1000, 'imgvotes')
    votes_list = leaders_votes["ID"].tolist()
    return votes_list

def create_if_not_exists(strname: str):
    if not os.path.exists(strname):
        os.makedirs(strname)
    return

def check_nsfw(filename: str,threshold: float):
    r = requests.post(
    "https://api.deepai.org/api/nsfw-detector",
    files={
        'image': open(f'{const_path}/{filename}', 'rb'),
    },
    headers={'api-key': keyring.api_key}
    )
    result = (dict(r.json()))
    if 'err' in result:
        print("error")
        return
    else:    
        score = float((result['output']['nsfw_score']))
        if score > threshold:
            arg = 'unsafe'
        else:
            arg = 'safe'
        copy(f'{const_path}/{filename}', f'outputs/{arg}/{filename}')    
        return score        


def parse(filename_list):
    create_if_not_exists('outputs/safe/')
    create_if_not_exists('outputs/unsafe/')
    if os.path.exists('csvfile.csv'):
        os.remove('csvfile.csv')
    dirs = os.listdir(const_path)
    for filename in dirs:
        file_no_ext = filename[:-4]
        if file_no_ext in filename_list:
            if filename[-3:] == 'jpg' or filename[-3:] == 'jpeg':
                data_dict = {}
                data_dict[filename] = check_nsfw(filename,0.5)
                with open('csvfile.csv','a') as file:
                    data = file_no_ext + "," + str(data_dict[filename])
                    file.write(data)
                    file.write('\n')
      


if __name__ == "__main__":
    df = pd.read_csv(meta_path)
    top_voted_img_names = find_top(df)
    parse(top_voted_img_names)

