import requests
import os
import wget
from shutil import move,copy
import csv
import pandas as pd
import keys as keyring
from datetime import date
import numpy as np

dict_to_months = {
    "5 years ago": "60",
    "8 years ago": "96",
    "4 years ago": "48",
    "3 years ago": "36",
    "7 years ago": "84",
    "2 years ago": "24",
    "1 year ago": "12",
    "12 months ago": "12",
    "11 months ago": "11",
    "9 years ago": "108",
    "10 months ago": "10",
    "9 months ago": "9",
    "8 months ago": "8",
    "7 months ago": "7",
    "6 months ago": "6",
    "5 months ago": "5",
    "4 months ago": "4",
    "3 months ago": "3",
    "2 months ago": "2",
    "1 month ago": "1",
    "4 weeks ago": "1",
    "3 weeks ago": "0.75",
    "2 weeks ago": "0.5",
    "1 week ago": "0.25",
    "6 years ago": "72"
}

debug = True

meta = 'meta'
meta_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', meta))

df1 = pd.read_csv(f'{meta_path}\\memes.csv')

df2 = pd.read_csv(f'{meta_path}\\csvfile.csv')
df2.columns = ['ID', 'nsfw_score']

df_total = pd.merge(df1, df2, on='ID', how='outer')

df_total = df_total.replace({"memedate": dict_to_months})
df_total = df_total.drop(df_total[df_total.memedate == 'no date'].index)

df_total['currdate']  = pd.to_datetime('today')
df_total['memedate'] = df_total['memedate'].astype(float).apply(np.ceil).astype(int)
df_total.to_csv(f'{meta_path}\\memes_plus_nsfw_index.csv')
df_total['meme_abs_date'] = df_total['currdate'] -  pd.to_timedelta(df_total['memedate'], unit='M')
df_total.to_csv(f'{meta_path}\\memes_plus_nsfw_index.csv')