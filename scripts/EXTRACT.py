import pandas as pd
import numpy as np
import os
from shutil import move

meta_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'meta'))

#locate data, convert to csv, cleanup a bit
def main():
    df_original = pd.read_excel(meta_path+'\mems metadata.xlsx', sheet_name='Sheet1', engine='openpyxl')
    df_clean = df_original.replace(np.nan, '', regex=True)
    df_clean.to_csv('memes.csv', index=False)
    move(f'memes.csv', f'{meta_path}\memes.csv')  

if __name__ == "__main__":
    main()